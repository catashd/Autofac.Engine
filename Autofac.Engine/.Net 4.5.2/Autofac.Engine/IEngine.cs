﻿using Autofac.Engine.Configuration;
using Autofac.Engine.DependencyManagement;
using System;
namespace Autofac.Engine
{
    /// <summary>
    /// Classes implementing this interface can serve as a portal for the 
    /// various services composing the Ioc engine. Edit functionality, modules
    /// and implementations access most Ioc functionality through this 
    /// interface.
    /// </summary>
    public interface IEngine
    {
        /// <summary>
        /// Container manager
        /// </summary>
        ContainerManager ContainerManager { get; }

        /// <summary>
        /// Initialize components and plugins in the Ioc environment.
        /// </summary>
        /// <param name="config">Config</param>
        void Initialize(IocConfig config);

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <returns></returns>
        T Resolve<T>() where T : class;

        /// <summary>
        ///  Resolve dependency
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns></returns>
        object Resolve(Type type);

        /// <summary>
        /// Resolve dependencies
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <returns></returns>
        T[] ResolveAll<T>();
    }
}
