﻿using System;
using System.Collections.Generic;

namespace Autofac.Engine
{
    public static class IComponentContextExtensions
    {
        public static object ResolveUnregistered(this IComponentContext context, Type type)
        {
            var constructors = type.GetConstructors();
            foreach (var constructor in constructors)
            {
                try
                {
                    var parameters = constructor.GetParameters();
                    var parameterInstances = new List<object>();
                    foreach (var parameter in parameters)
                    {
                        var service = context.Resolve(parameter.ParameterType);
                        if (service == null) throw new IocException("Unkown dependency");
                        parameterInstances.Add(service);
                    }
                    return Activator.CreateInstance(type, parameterInstances.ToArray());
                }
                catch (IocException)
                {

                }
            }
            throw new IocException("No contructor was found that had all the dependencies satisfied.");
        }
    }
}
