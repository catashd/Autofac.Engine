﻿using System;
using System.Runtime.Serialization;

namespace Autofac.Engine
{
    [Serializable]
    public class IocException : Exception
    {
        public IocException(string message) : base(message) { }

        public IocException(string messageFormat, params object[] args) : base(string.Format(messageFormat, args)) { }

        protected IocException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public IocException(string message, Exception innerException) : base(message, innerException) { }
    }
}
