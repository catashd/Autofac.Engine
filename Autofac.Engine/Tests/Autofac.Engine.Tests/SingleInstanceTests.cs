﻿using NUnit.Framework;
using Lc.Tests;
using Autofac;
using Autofac.Engine;

namespace Lc.Autofac.Engine.Tests
{
    [TestFixture]
    public class SingleInstanceTests
    {
        #region Fields
        #endregion

        #region SetUp
        [SetUp]
        public void SetUp()
        {
            EngineContext.Initialize(false);
        }
        #endregion

        #region Methods
        [Test]
        public void Can_resolve_singleinstance()
        {
            ICustomerService _customerService = EngineContext.Resolve<ICustomerService>();
            var count = _customerService.Count();
            (count == 90).ShouldBeTrue(string.Format("count should be equal to 90, but result is {0}", count));
        }

        [Test]
        public void Can_resolve_singleinstance2()
        {
            ICustomerService _customerService = EngineContext.Resolve<ICustomerService>();
            var count = _customerService.Count();
            (count == 89).ShouldBeTrue(string.Format("count should be equal to 89, but result is {0}", count));
        }
        #endregion


        #region Registrar

        public interface ICustomerService
        {
            int Count();
        }
        public class CustomerService : ICustomerService
        {
            private int count = 90;
            public int Count()
            {
                return count--;
            }
        }
        public class DependencyRegistrar : IDependencyRegistrar
        {
            public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
            {
                builder.RegisterType<CustomerService>().As<ICustomerService>().SingleInstance();
            }

            public int Order { get { return 0; } }
        }
        #endregion
    }
}
