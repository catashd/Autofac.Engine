﻿using NUnit.Framework;
using Lc.Tests;
using Autofac;
using Autofac.Engine;
namespace Lc.Autofac.Engine.Tests
{
    [TestFixture]
    public class InstancePerDependencyTests
    {
        #region Fields
        #endregion

        #region SetUp
        [SetUp]
        public void SetUp()
        {
            EngineContext.Initialize(false);
        }
        #endregion

        #region Methods
        [Test]
        public void Can_resolve_perdependency()
        {
            IOrderService _customerService = EngineContext.Resolve<IOrderService>();
            var count = _customerService.Count();
            (count == 90).ShouldBeTrue(string.Format("count should be equal to 90, but result is {0}", count));
        }

        [Test]
        public void Can_resolve_perdependency2()
        {
            IOrderService _customerService = EngineContext.Resolve<IOrderService>();
            var count = _customerService.Count();
            (count == 90).ShouldBeTrue(string.Format("count should be equal to 90, but result is {0}", count));
        }
        #endregion

        #region Registrar

        public interface IOrderService
        {
            int Count();
        }
        public class OrderService : IOrderService
        {
            private int count = 90;
            public int Count()
            {
                return count--;
            }
        }
        public class DependencyRegistrar : IDependencyRegistrar
        {
            public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
            {
                builder.RegisterType<OrderService>().As<IOrderService>().InstancePerDependency();
            }

            public int Order { get { return 0; } }
        }
        #endregion
    }
}
